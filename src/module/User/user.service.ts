import { Injectable } from '@nestjs/common';
import { ERole, UserEntity } from 'src/db/entities/user.entity';
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

@Injectable()
export class UserService {
  async getAllUsers() {
    const users = await UserEntity.find();
    return users;
  }

  async createUser(name: string, email: string, password: string, role: ERole) {
    try {
      const doesUserExist = await UserEntity.findOne({
        where: {
          email,
        },
      });
      if (doesUserExist) throw new Error('User already exists');

      const Pass = await bcrypt.hash(
        password,
        parseInt(process.env.SALT_ROUNDS),
      );

      await UserEntity.insert({
        name,
        email,
        password: Pass,
        role,
      });
      return 'User created successfully';
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  async login(email: string, password: string) {
    try {
      const user = await UserEntity.findOne({
        where: {
          email,
        },
      });

      if (!user) throw new Error('Wrong email address');

      const passwordMatched = await bcrypt.compare(password, user.password);
      if (!passwordMatched) throw new Error('Wrong password');

      const token = jwt.sign(
        { id: user.id, role: user.role },
        process.env.JWT_SECRET,
        {
          expiresIn: '1h',
        },
      );
      return token;
    } catch (e) {
      return e;
    }
  }

  async getProfile(id: any) {
    try {
      const user = await UserEntity.findOne(id);
      if (!user) throw new Error("User doesn't exists");
      return {
        id: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
      };
    } catch (e) {
      return e;
    }
  }
}
