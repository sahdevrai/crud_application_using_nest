import { ERole } from 'src/db/entities/user.entity';

const Joi = require('joi');

export const createUserSchema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  role: Joi.string().valid(ERole.admin, ERole.customer),
});
