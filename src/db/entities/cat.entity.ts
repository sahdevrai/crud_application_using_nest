import {Entity, 
    PrimaryGeneratedColumn,
     Column,
     BaseEntity} from "typeorm";

@Entity()
  export class CatEntity extends BaseEntity{

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name:string;

    @Column()
    age:number;

    @Column()
    breed:string;

    
  }   