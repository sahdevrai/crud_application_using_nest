const Joi = require('joi');

export const catAgeRangeSchema = Joi.object({
  age_lte: Joi.number().required(),
  age_gte: Joi.number().required(),
});
