import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from './auth.guard';

@Injectable()
export class RolesGuard extends AuthGuard implements CanActivate {
  constructor(private reflector: Reflector) {
    super();
  }

  async canActivate(context: ExecutionContext) {
    const isAuthorized = await super.canActivate(context);
    if (!isAuthorized) return false;

    const roles = this.reflector.get<string>('role', context.getHandler());
    if (!roles) {
      return true;
    }

    const request = context.switchToHttp().getRequest();
    const userRole = request.headers.user.role;
    return roles.includes(userRole);
  }
}
