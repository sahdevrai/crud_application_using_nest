import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
const jwt = require('jsonwebtoken');

@Injectable()
export class AuthGuard implements CanActivate {
  async validateRequest(req: any) {
    try {
      const token = req.headers.jwt;
      if (!token) return false;
      const decoded = jwt.verify(token, process.env.JWT_SECRET);
      req.headers.user = { id: decoded.id, role: decoded.role };
      return decoded ? true : false;
    } catch (e) {
      return false;
    }
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    return this.validateRequest(request);
  }
}
